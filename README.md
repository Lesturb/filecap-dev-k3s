# FileCap development K3s cluster
This repository contains the base documentation and definitions for the FileCap development K3s cluster. The cluster consists of a K3s installation without the default loadbalancer (KlipperLB) and without the default reverse proxy (Traefik). To replace the loadbalancer, we have chosen MetalLB as this allows us to assign a pool of IP addresses that we can use to expose services. And for the reverse proxy we have opted to deploy Traefik in a more flexible manner.

## K3s setup
### Master nodes
#### Initial master
As root execute the following on the initial master node, after the reboot wait for it to comeback and continue with the next nodes.
```
curl -sfL https://get.k3s.io | sh -s - --cluster-init --disable servicelb --disable traefik
cat /var/lib/rancher/k3s/server/token # Store this token for the secondary master setup
systemctl reboot
```
#### Secondary masters
```
curl -sfL https://get.k3s.io | K3S_TOKEN=<K3S_TOKEN> sh -s - server --server https://<INITIAL_MASTER_IP>:6443 --disable servicelb --disable traefik
systemctl reboot
```
### Agent nodes
```
curl -sfL https://get.k3s.io | K3S_URL=https://<INITIAL_MASTER_IP>:6443 K3S_TOKEN=<K3S_TOKEN> sh -s -
```
### MetalLB
Install the helm chart
```
kubectl create namespace metallb-system
helm install metallb -n metallb-system oci://registry-1.docker.io/bitnamicharts/metallb
```
Apply the advertisement and pool definitions.
```
kubectl apply -f metallb/
```
### FleetLock
We use FleetLock to coordinate the reboots of our nodes. This requires each node to request a reboot lock from the coordinator in order for it to reboot. In this way we ensure that not every node reboots at the same time after applying updates.
```
kubectl apply -f fleetlock/
```

### Traefik
Configure the Traefik Helm repo and install the Traefik chart.
```
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
kubectl create namespace traefik-system
helm install -n traefik-system -f traefik/00_traefik-values.yaml traefik traefik/traefik
```
### cert-manager
We have chosen to use cert-manager to request and issue certificates for exposed services on this cluster. Our base configuration creates a cluster issuer that uses Let's Encrypt with the HTTP-01 challenge.
```
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.1/cert-manager.crds.yaml
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.13.1
```
Apply the cert-manager definitions
```
kubectl apply -f cert-manager/
```
### Exposing Traefik
We have chosen to use the *.dev.filecap.net subdomain for exposed services running on this cluster. Therefore we have created an A-record that points to the firewall of the FileCap dev environment. In this firewall we have configured DNAT-rules that will forward traffic destined for 87.249.117.80 to the internal Traefik IP of 10.150.114.100.

### Traefik dashboard
Apply the Traefik dashboard definitions
```
kubectl apply -f traefik/{01_traefik-tls-options-mtls.yaml,02_traefik-certificate.yaml,03_traefik-ingressroute.yaml}
```

### Whoami - Demo service
To demonstrate the functionality of what we just setup, we deploy the Traefik whoami service.
```
kubectl apply -f whoami/
```

Now navigate to https://whoami.dev.filecap.net/